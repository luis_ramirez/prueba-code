# -*- coding: utf-8 -*-
"""
This is a config file por enviroment variable definitions

Example:
    import and inherit Class::

        import config

Class Config define global and enviromental Variables.

Todo:
    * For module TODOs
    * You have to also use ``sphinx.ext.todo`` extension
"""

import os
import pymongo
from pymongo import MongoClient
from dotenv import load_dotenv
import datetime

class Config():
    '''
        Simple config class to be heritated
    '''
    APP_NAME = "Prueba API"
    MONGO_URI = os.getenv("MONGO_URI")
    PRODUCTS_COLLECTION = os.getenv("PRODUCTS_COLLECTION")
    DB_NAME = os.getenv("DB_NAME")
    


class ProductionConfig(Config):
    DEBUG = False

class DevelopmentConfig(Config):
    ALGO = "TURURÚ"
    DEBUG = True

class TestConfig(Config):
    TESTING = True