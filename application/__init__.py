from flask import Flask
from application import config
from flask_cors import CORS

# app = Flask(__name__)
# enviroment = app.config["ENV"]

def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    enviroment = app.config["ENV"]
    # Application Configuration
    if enviroment == "production":
        app.config.from_object(config.ProductionConfig)
    if enviroment == "development":
        app.config.from_object(config.DevelopmentConfig)
    if enviroment == "test":
        app.config.from_object(config.TestConfig)
        app.config.from_object('config.Config')

    with app.app_context():
        # Import parts of our application
        from .products.views import valudate_products_app, retrieve_products_app
        app.register_blueprint(valudate_products_app, url_prefix='/api')
        app.register_blueprint(retrieve_products_app, url_prefix='/api')
    cors = CORS(app)
    return app