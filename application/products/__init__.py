from pymongo import MongoClient
from flask import current_app as app

client = MongoClient(app.config["MONGO_URI"], ssl=True)
db = client.get_database(app.config["DB_NAME"])
products_collection = db[app.config["PRODUCTS_COLLECTION"]]