from . import products_collection

class Product:
    def __init__(self, *args, **kwargs):
        self.errors = []

    def validate_product(self, product):
        try:
            self.id = product["id"]
        except:
            self.errors.append("invalid id")
        try:
            if product["name"] and (len(product["name"]) > 3 and len(product["name"]) < 55):
                self.name = product["name"]
            else:
                self.errors.append("Invalid product name")
        except:
            self.errors.append("Invalid product name")
        try:
            if product["value"] and (product["value"] > 0 and product["value"] < 99999.9):
                self.value = product["value"]
            else:
                self.errors.append("Invalid value")
        except:
            self.errors.append("Invalid value")
        try:
            if product["discount_value"] < product["value"]:
                self.discount_value = product["discount_value"]
            else:
                self.errors.append("Invalid discount value")
        except:
            pass
        
        try:
            if product["stock"] and product["stock"] > 0:
                self.stock = product["stock"]
            else:
                self.errors.append("Invalid stock value")
        except:
            self.errors.append("Invalid stock value")

    def __repr__(self):
        return str(self.__dict__)

    def __save__(self):
        del self.__dict__["errors"]
        result = products_collection.update({"id": self.id}, self.__dict__, upsert=True)
        