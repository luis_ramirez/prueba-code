from flask.views import MethodView
from flask import request, abort, json, jsonify
from .model import Product
from . import products_collection

class ValidateProductsAPI(MethodView):
    def post(self):
        errors = []
        products = request.json.get("products", [])
        if products == []:
            return jsonify("Empty product list"), 422 
        for product in products:
            try:
                aux = Product()
                aux.validate_product(product)
                if aux.errors == []:
                    aux.__save__()
                else:
                    errors.append({
                        "product_id": aux.id,
                        "errors": aux.errors
                    })
            except ValueError as err:
                return jsonify(":("), 422 
        if (errors == []):
            return jsonify({"status": "OK"}), 200 
        else:
            return jsonify({
                "status": "ERROR",
                "products_report": errors,
                "_number_of_products_unable_to_parse": len(errors)
            })

class RetrieveProductsAPI(MethodView):
    def get(self):
        try:
            result = products_collection.find({}, {"_id": False})
            return jsonify({"products": list(result)})
        except ValueError as err:
            return jsonify("Error en el servidor, intente más tarde"), 500