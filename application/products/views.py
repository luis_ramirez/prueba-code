from flask import Blueprint
from .api import ValidateProductsAPI, RetrieveProductsAPI

valudate_products_app = Blueprint('valudate_products_app', __name__)
valudate_products_view = ValidateProductsAPI.as_view('valudate_products_view')
valudate_products_app.add_url_rule('products/bulk_insert',
                        view_func=valudate_products_view,
                        methods=['POST'])

retrieve_products_app = Blueprint('retrieve_products_app', __name__)
retrieve_products_view = RetrieveProductsAPI.as_view('valretrieve_products_viewudate_products_view')
retrieve_products_app.add_url_rule('products',
                        view_func=retrieve_products_view,
                        methods=['GET'])