### API FOR DACODES INTERVIEW

This api provides a service for product saving and query.

It's Flask app so in order to run the code locally you need to follow the next steps

Clone this repo to your local machine. In the top level directory, create a virtual environment:
```
$ python -m venv .venv
$ source .venv/bin/activate
```
Now install the required modules:
```
$ pip install -r requirements.txt
```
Run the app with:
```
$ flask run
```

The api is available in http://0.0.0.0:5000

Contains two main endpoints
One for saving products with the following format:
```
{
    "products": [
        {
            "discount_value": 30,
            "id": "qafasd23asdfase",
            "name": "Computer",
            "stock": 10,
            "value": 1000
        }
    ]
}
```
Corresponds to:
```
Method: POST
Endpoint: /api/products/bulk_insert
``` 
And the second one is a way of consulting all of products in the DB
Corresponds to:
```
Method: GET
Endpoint: /api/products
``` 